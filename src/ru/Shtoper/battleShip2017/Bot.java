package ru.Shtoper.battleShip2017;

import java.util.*;

/**
 * Created by Роман on 03.03.2017.
 * Бот для игры против компьютера
 */
public class Bot extends Player {

    private Set<Point> botShoots; // сохранение позиций ударов бота
    private Stack<Point> woundedPlayerShips; // сохранение позиций раненых ботом кораблей
    private ArrayList<Point> tempNeighborsWounded; //возможные соседи подбитого корабля
    private LinkedList<Point> unshoted; //нестреленные позиции

    public ResultOfShot resultOfLastShot;
    private Point pointOfLastShot;

    public Bot() {
        botShoots = new HashSet<>();
        unshoted = new LinkedList<>();
        woundedPlayerShips = new Stack<>();
        tempNeighborsWounded = new ArrayList<>();
        resultOfLastShot = ResultOfShot.MISSED;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                unshoted.add(new Point(j, i));
            }
        }
    }

    @Override
    public void prepareToNewGame() {
        botShoots.clear();
        woundedPlayerShips.clear();
        tempNeighborsWounded.clear();
        if (unshoted.size() < 100) {
            unshoted.clear();
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    unshoted.add(new Point(j, i));
                }
            }
        }

    }


    public Point doShot() { // выбор позиции для выстрела
        Random random = new Random();
        Point point;
        if (resultOfLastShot == ResultOfShot.WOUNDED)
            woundedPlayerShips.add(pointOfLastShot);
        else if (resultOfLastShot == ResultOfShot.KILLED) { // отмечаем соседей убитого корабля как стреленные координаты
            woundedPlayerShips.add(pointOfLastShot);
            Set<Point> neighbors = new HashSet<>();
            for (Point p : woundedPlayerShips) { //Добавление соседей
                if (p.getX() - 1 >= 0) {
                    neighbors.add(new Point(p.getX() - 1, p.getY()));
                    if (p.getY() - 1 >= 0)
                        neighbors.add(new Point(p.getX() - 1, p.getY() - 1));
                    if (p.getY() + 1 < FIELD_SIZE)
                        neighbors.add(new Point(p.getX() - 1, p.getY() + 1));
                }
                if (p.getX() + 1 < FIELD_SIZE) {
                    neighbors.add(new Point(p.getX() + 1, p.getY()));
                    if (p.getY() - 1 >= 0)
                        neighbors.add(new Point(p.getX() + 1, p.getY() - 1));
                    if (p.getY() + 1 < FIELD_SIZE)
                        neighbors.add(new Point(p.getX() + 1, p.getY() + 1));
                }
                if (p.getY() - 1 >= 0)
                    neighbors.add(new Point(p.getX(), p.getY() - 1));
                if (p.getY() + 1 < FIELD_SIZE)
                    neighbors.add(new Point(p.getX(), p.getY() + 1));
            }
            for(Point p : neighbors) {
                botShoots.add(p);
                unshoted.remove(p);
            }
            woundedPlayerShips.clear();
            tempNeighborsWounded.clear();
        }
        if (woundedPlayerShips.empty()) { // Если нет раненых кораблей, то берем случайную позицию из нестреленных
            int randomInt = random.nextInt(unshoted.size());
            point = unshoted.get(randomInt);
            unshoted.remove(randomInt);
            botShoots.add(point);
            pointOfLastShot = point;
            return point;
        } else { // если есть раненый корабль, то стреляем по соседним клеткам
            if (woundedPlayerShips.size() == 1) { //неизвестна оринтация корабля, возможен любой сосед
                Point p = woundedPlayerShips.firstElement();
                if (p.getX() - 1 >= 0)
                    tempNeighborsWounded.add(new Point(p.getX() - 1, p.getY()));
                if (p.getX() + 1 < FIELD_SIZE)
                    tempNeighborsWounded.add(new Point(p.getX() + 1, p.getY()));
                if (p.getY() - 1 >= 0)
                    tempNeighborsWounded.add(new Point(p.getX(), p.getY() - 1));
                if (p.getY() + 1 < FIELD_SIZE)
                    tempNeighborsWounded.add(new Point(p.getX(), p.getY() + 1));
            } else { //известна оринтация корабля
                tempNeighborsWounded.clear();
                boolean shipIsVertical = (woundedPlayerShips.get(0).getX() == woundedPlayerShips.get(1).getX());
                if (shipIsVertical) { // если подбитый корабль вертикальный
                    for (Point p : woundedPlayerShips) { //добавляем в возможные соседи только смещение по у
                        if (p.getY() - 1 >= 0)
                            tempNeighborsWounded.add(new Point(p.getX(), p.getY() - 1));
                        if (p.getY() + 1 < 10)
                            tempNeighborsWounded.add(new Point(p.getX(), p.getY() + 1));
                    }
                } else { // если подбитый корабль горизонтальный
                    for (Point p : woundedPlayerShips) { //добавляем в возможные соседи только смещение по х
                        if (p.getX() - 1 >= 0)
                            tempNeighborsWounded.add(new Point(p.getX() - 1, p.getY()));
                        if (p.getX() + 1 < 10)
                            tempNeighborsWounded.add(new Point(p.getX() + 1, p.getY()));
                    }
                }
            }
            do {
                point = tempNeighborsWounded.get(random.nextInt(tempNeighborsWounded.size())); // шмаляем в любого соседа раненого
            }
            while (!unshoted.contains(point));
            tempNeighborsWounded.remove(point);
        }
        botShoots.add(point);
        unshoted.remove(point);
        pointOfLastShot = point;
        return point;
    }
}
