package ru.Shtoper.battleShip2017;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by Роман on 04.03.2017.
 * Главный контроллер для связи логики и отображения
 */
public class GameController implements Initializable {
    @FXML
    GridPane playerPane;
    @FXML
    GridPane botPane;
    @Override
    public void initialize(URL location, ResourceBundle resources){
        botPane.setDisable(true);
        showCells.put(ResultOfShot.KILLED, Color.RED);
        showCells.put(ResultOfShot.WOUNDED, Color.YELLOW);
        showCells.put(ResultOfShot.MISSED, Color.WHITE);
        user = new User();
        bot = new Bot();

    }
    static Map<Player, GridPane> panes;

    Map <ResultOfShot, Paint> showCells = new HashMap<>();

    User user;
    Bot bot;







    public void showShips(Player player) {
        GridPane gridPane = panes.get(player);

        for (Ship ship : player.fleet)
        {
            for (Point point : ship.positions){
                ((Rectangle)gridPane.getChildren().get(point.getX()*10 + point.getY())).setFill(Color.DODGERBLUE);
            }
        }
    }

    public void hideShips(Player... players) {
        for(Player player : players) {
            GridPane gridPane = panes.get(player);

            for (Node node : gridPane.getChildren())
            {
                if(node instanceof Rectangle) {
                    Rectangle r = (Rectangle) node;
                    r.setFill(Color.AQUAMARINE);
                }
            }
        }
    }



    public void startTheGame(MouseEvent mouseEvent) {
        bot.prepareToNewGame();
        user.createFleet();
        bot.createFleet();

        if (panes == null) { // В первую игру создаем Map`у <Игрок-отображаемое поле>
            panes = new HashMap<>();
            panes.put(user, playerPane);
            panes.put(bot, botPane);
        }

        hideShips(user, bot);
        showShips(user);
        botPane.setDisable(false);
    }
    public void checkPlayer(Player player) {
        if (player.countOfShips == 0) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            String text, title;
            if (player instanceof Bot) { // Bot - проигравший
                title = "Победа!";
                text = "Поздравляем, вы победили, игра окончена";
            } else {
                title = "Вы проиграли!";
                text = "К сожалению, вы проиграли";
            }
            alert.setTitle(title);
            alert.setHeaderText(text);
            alert.setContentText("Можете сыграть ещё");
            alert.show();

            botPane.setDisable(true);

        }
    }

    private void showResultsOfShot(Player player, Point point, ResultOfShot resultOfShot) {
        GridPane pane = panes.get(player);
        Paint colorOfCell = showCells.get(resultOfShot);
        switch (resultOfShot) {
            case KILLED:
                for(Ship ship : player.fleet) {
                    if (ship.positions.contains(point)) {
                        for (Point pointOfPosition : ship.positions) {
                            ((Rectangle)pane.getChildren().get(pointOfPosition.getX()*10 + pointOfPosition.getY())).setFill(colorOfCell); //отмечаем все позиции корабля "убит"
                        }
                        for (Point pointOfNeighbors : ship.neighbors) {
                            ((Rectangle)pane.getChildren().get(pointOfNeighbors.getX()*10 + pointOfNeighbors.getY())).setFill(showCells.get(ResultOfShot.MISSED)); //отмечаем все позиции соседей корабля "мимо"
                        }
                    }
                }
                break;
            case WOUNDED:
                ((Rectangle)pane.getChildren().get(point.getX()*10 + point.getY())).setFill(colorOfCell);
                break;
            case MISSED:
                ((Rectangle)pane.getChildren().get(point.getX()*10 + point.getY())).setFill(colorOfCell);
                break;
        }
    }

    public void botPaneCliked(MouseEvent mouseEvent) { //обработка удара User`а - работает только после startTheGame()
        Rectangle rectangle = (Rectangle)mouseEvent.getSource();
        GridPane pane = (GridPane) rectangle.getParent();
        int y = pane.getRowIndex(rectangle);
        int x = pane.getColumnIndex(rectangle);
        Point currentUserShoot =  new Point(x, y);
        ResultOfShot result = bot.handlingShot(currentUserShoot);
        showResultsOfShot(bot, currentUserShoot, result);
        if (result == ResultOfShot.KILLED)
            checkPlayer(bot);

        Point currentBotShoot = bot.doShot();
        result = user.handlingShot(currentBotShoot);
        bot.resultOfLastShot = result;
        showResultsOfShot(user, currentBotShoot, result);
        if (result == ResultOfShot.KILLED)
            checkPlayer(user);
    }

    public void exit(MouseEvent mouseEvent) {
        System.exit(0);
    }
}
