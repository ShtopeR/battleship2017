package ru.Shtoper.battleShip2017;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Роман on 03.03.2017.
 */
public class Ship {
    private int size;
    private int health;
    boolean isVertical;
    ArrayList<Point> positions;
    Set<Point> neighbors;


    public Ship(int size) {
        this.size = size;
        this.health = size;
        positions = new ArrayList<>();
        neighbors = new HashSet<>();
    }

    public int getSize() {
        return size;
    }

    void hit() {
        health--;
    }
    public boolean isAlive() {
        return health > 0;
    }

    public void markNeighbors() {
        ArrayList<Point> temp = new ArrayList<>();
        Point firstPoint = positions.get(0);
        Point lastPoint = positions.get(positions.size()-1);
        if(isVertical) {
            for (Point p :positions) {
                temp.add(new Point((p.getX() - 1), p.getY()));
                temp.add(new Point((p.getX() + 1), p.getY()));
            }
            temp.add (new Point(firstPoint.getX()-1, firstPoint.getY()-1));
            temp.add (new Point(firstPoint.getX(), firstPoint.getY()-1));
            temp.add (new Point(firstPoint.getX()+1, firstPoint.getY()-1));
            temp.add (new Point(lastPoint.getX()-1, lastPoint.getY()+1));
            temp.add (new Point(lastPoint.getX(), lastPoint.getY()+1));
            temp.add (new Point(lastPoint.getX()+1, lastPoint.getY()+1));

        }
        else {
            for (Point p :positions) {
                temp.add(new Point(p.getX(), (p.getY() - 1)));
                temp.add(new Point(p.getX(), (p.getY() + 1)));
            }
            temp.add (new Point(firstPoint.getX()-1, firstPoint.getY()+1));
            temp.add (new Point(firstPoint.getX()-1, firstPoint.getY()));
            temp.add (new Point(firstPoint.getX()-1, firstPoint.getY()-1));
            temp.add (new Point(lastPoint.getX()+1, lastPoint.getY()-1));
            temp.add (new Point(lastPoint.getX()+1, lastPoint.getY()));
            temp.add (new Point(lastPoint.getX()+1, lastPoint.getY()+1));
        }
        for (Point p : temp) {
            if (p.getX() >= 0 && p.getY() >=0)
                neighbors.add(p);
        }

    }
}
