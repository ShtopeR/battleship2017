package ru.Shtoper.battleShip2017;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by Роман on 03.03.2017.
 * Базовый класс игрока
 */
public abstract class Player {

    Set<Ship> fleet;
    Set<Point> positionsOfShips; //для быстрой проверки попал/мимо
    protected int countOfShips;
    protected final int FIELD_SIZE = 10;

    public Player() {
        fleet = new HashSet<>();
        positionsOfShips = new HashSet<>();
    }

    public void createFleet() {
        countOfShips = 0;
        fleet.clear();
        positionsOfShips.clear();
        for ( int i = (int)(FIELD_SIZE * 0.4); i > 0; i--) { //i - размер корабля
            for (int j = i; j < FIELD_SIZE/2; j ++) { // j - кол-во кораблей
                Ship ship = new Ship(i);
                autoPlaceShip(ship);
                ship.markNeighbors();
                fleet.add(ship);
                countOfShips++;
            }
        }
    }

    public void autoPlaceShip(Ship ship) {
        int shipSize = ship.getSize();
        Random random = new Random();
        ship.isVertical = random.nextBoolean(); //случайно - вертикальный / горизонтальный
        int x, y;
        boolean shipNotPlaced = true;

        do {
            boolean isEmpty = true;
            Point tempPoint = null;
            if (ship.isVertical) { // если вертикальный - случайные координаты, с учетом размера корабля. Проверка по оси у
                x = random.nextInt(FIELD_SIZE);
                y = random.nextInt(FIELD_SIZE - (shipSize - 1));

                for (int i = 0, tempX = x, tempY = y; i < shipSize; i++, tempY++) { // проверка нужных ячеек на свободу
                    tempPoint = new Point(tempX, tempY);
                    for (Ship s : fleet) {
                        if (s.positions.contains(tempPoint) || s.neighbors.contains(tempPoint))
                            isEmpty = false;
                    }
                }
                if (isEmpty) { //если координаты, необходимые для корабля свободны
                    for (int i = 0; i < shipSize; i++, y++) { //Размещаем корабль, сохраняем его позиции
                        tempPoint = new Point(x, y);
                        ship.positions.add(tempPoint);
                        positionsOfShips.add(tempPoint);
                    }
                    shipNotPlaced = false;
                }

            } else { // если горизонтальный - случайные координаты, с учетом размера корабля. Проверка по оси х
                x = random.nextInt(FIELD_SIZE - (shipSize - 1));
                y = random.nextInt(FIELD_SIZE);
                for (int i = 0, tempX = x, tempY = y; i < shipSize; i++, tempX++) { // проверка нужных ячеек на свободу
                    tempPoint = new Point(tempX, tempY);
                    for (Ship s : fleet) {
                        if (s.positions.contains(tempPoint) || s.neighbors.contains(tempPoint))
                            isEmpty = false;
                    }
                }
                if (isEmpty) { //если координаты, необходимые для корабля свободны
                    for (int i = 0; i < shipSize; i++, x++) { //Размещаем корабль, сохраняем его позиции
                        tempPoint = new Point(x, y);
                        ship.positions.add(tempPoint);
                        positionsOfShips.add(tempPoint);
                    }
                    shipNotPlaced = false;
                }
            }
        }
        while (shipNotPlaced);
    }

    public ResultOfShot handlingShot(Point shotPoint) {
        if (!positionsOfShips.contains(shotPoint)) // если промах
            return ResultOfShot.MISSED;
        else { //если попал
            ResultOfShot resultOfShot = null;
            for (Ship ship : fleet) {
                if (ship.positions.contains(shotPoint)) { // найден подбитый корабль
                    ship.hit();
                    if (!ship.isAlive()) { // если корабль убит
                        countOfShips--;
                        resultOfShot = ResultOfShot.KILLED;
                    }
                    else resultOfShot = ResultOfShot.WOUNDED;
                }
            }
            return resultOfShot;
        }
    }

    public abstract void prepareToNewGame();

}
