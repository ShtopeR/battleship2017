package ru.Shtoper.battleShip2017;

/**
 * Created by Роман on 03.03.2017.
 * Класс позиции кораблей и выстрелов
 */
public class Point {
    private int x = -1;
    private int y = -1;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point(int x, int y) {
        if (x >= 0 && x < 10)
            this.x = x;
        if (y >= 0 && y < 10)
            this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (x != point.x) return false;
        return y == point.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
